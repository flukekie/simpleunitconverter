var pSizeW = [841, 594, 420, 297, 210];
var pSizeH = [1189, 841, 594, 420, 297];

function init() {
    paperH();
}

function radiohook() {

    $("input[name='options']").change(
        function () {
            var value = $("input[name='options']:checked").val();
            if (value == "H") {
                paperH()
            }
            if (value == "V") {
                paperV()
            }
        });
}

function paperH() {

    var hSide = pSizeH;
    var wSide = pSizeW;

    $('.orientation').text("Hor");
    $('.a4-h').text(hSide[4]);
    $('.a3-h').text(hSide[3]);
    $('.a2-h').text(hSide[2]);
    $('.a1-h').text(hSide[1]);
    $('.a0-h').text(hSide[0]);

    $('.a4-w').text(wSide[4]);
    $('.a3-w').text(wSide[3]);
    $('.a2-w').text(wSide[2]);
    $('.a1-w').text(wSide[1]);
    $('.a0-w').text(wSide[0]);

    $(".paper").css({
        "width": "16.5em",
        "height": "11.7em"
    });
}

function paperV() {

    var hSide = pSizeW;
    var wSide = pSizeH;
    $('.orientation').text("Ver");
    $('.a4-h').text(hSide[4]);
    $('.a3-h').text(hSide[3]);
    $('.a2-h').text(hSide[2]);
    $('.a1-h').text(hSide[1]);
    $('.a0-h').text(hSide[0]);

    $('.a4-w').text(wSide[4]);
    $('.a3-w').text(wSide[3]);
    $('.a2-w').text(wSide[2]);
    $('.a1-w').text(wSide[1]);
    $('.a0-w').text(wSide[0]);

    $(".paper").css({
        "width": "11.7em",
        "height": "16.5em"
    });
}