function converter(source, num) {
    num = parseFloat(num);
    var i_ft = document.getElementById("i_ft");
    var i_m = document.getElementById("i_m");
    var i_in = document.getElementById("i_in");
    var i_cm = document.getElementById("i_cm");
    var i_mm = document.getElementById("i_mm");
    switch (source) {
        case "i_ft":
            {
                i_m.value = (num / 3.2808).toFixed(3);
                i_in.value = (num * 12).toFixed(3);
                i_cm.value = (num / 0.032808).toFixed();
                i_mm.value = (num / 0.0032808).toFixed();
                break;
            }
        case "i_m":
            {
                i_ft.value = (num * 3.2808).toFixed(3);
                i_in.value = (num * 39.370).toFixed(3);
                i_cm.value = (num / 0.01).toFixed();
                i_mm.value = (num / 0.001).toFixed();
                break;
            }
        case "i_in":
            {
                i_ft.value = (num * 0.083333).toFixed(3);
                i_m.value = (num / 39.370).toFixed(3);
                i_cm.value = (num / 0.39370).toFixed(2);
                i_mm.value = (num / 0.039370).toFixed();
                break;
            }
        case "i_cm":
            {
                i_ft.value = (num * 0.032808).toFixed(3);
                i_m.value = (num / 100).toFixed(3);
                i_in.value = (num * 0.39370).toFixed(2);
                i_mm.value = (num * 10).toFixed();
                break;
            }
        case "i_mm":
            {
                i_ft.value = (num * 0.0032808).toFixed(3);
                i_m.value = (num / 1000).toFixed(3);
                i_in.value = (num * 0.039370).toFixed(3);
                i_cm.value = (num / 10).toFixed(2);
                break;
            }
    }
}
